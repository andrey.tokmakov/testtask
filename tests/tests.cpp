//============================================================================
// Name        : main.cpp
// Created on  : 17.08.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Boost Project
//============================================================================

#include "Utilities.h"
#include "FootballTournament.h"
#include <gtest/gtest.h>

template<typename T>
::testing::AssertionResult IsBetweenInclusive(T val, T a, T b) {
    if ((val >= a) && (val <= b))
        return ::testing::AssertionSuccess();
    else
        return ::testing::AssertionFailure() << val << " is outside the range " << a << " to " << b;
}

TEST(TournamentTests, GoalsScored) {
    const std::map<uint32_t, double> distribution {
            {0, 0.2}, {1, 0.4}, {2, 0.6}, {3, 0.7}, {4, 0.85}, {5, 1.0}
    };

    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, -0.40), 0);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, -0.00), 0);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.00), 0);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.10), 0);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.20), 1);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.39), 1);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.80), 4);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.849), 4);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.851), 5);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.90), 5);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 0.99), 5);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 1.00), 5);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 1.01), 5);
    ASSERT_EQ(FootballTournament::getGoalsScored(distribution, 1.90), 5);
}

TEST(UtilitiesTests, GetRandomNumber) {
    using TestData = std::vector<std::tuple<double, double >>;
    for (const auto& [from, until]: TestData{
        {1.0, 2.0}, {0.0, 1.0}, {-1.0, 1.0}, {0.5, 2.5}, {-100.0, 100.0}, {-0.0, 0.0}
    }) {
        EXPECT_TRUE(IsBetweenInclusive(Utilities::randomDouble(from, until), from, until));
    }
}

TEST(UtilitiesTests, PointsCalcStrategyTest) {
    const std::unique_ptr<Utilities::IPointsCalcStrategy> strategy {
        std::make_unique<Utilities::PointsCalcStrategyImpl>()
    };

    // Testing the victory of the second team:
    // We expect that 3 points will be awarded to the second team and 0 points to the first
    ASSERT_TRUE( (std::pair<size_t, size_t>{0, 3}) == strategy->getPoints(1, 2));
    ASSERT_TRUE( (std::pair<size_t, size_t>{0, 3}) == strategy->getPoints(0, 1));

    // We are testing the situation when both teams played in a draw: one point each
    ASSERT_TRUE( (std::pair<size_t, size_t>{1, 1}) == strategy->getPoints(3, 3));
    ASSERT_TRUE( (std::pair<size_t, size_t>{1, 1}) == strategy->getPoints(0, 0));

    // Testing the victory of the first team:
    // We expect that 0 points will be awarded to the second team and 3 points to the first
    ASSERT_TRUE( (std::pair<size_t, size_t>{3, 0}) == strategy->getPoints(4, 2));
    ASSERT_TRUE( (std::pair<size_t, size_t>{3, 0}) == strategy->getPoints(1, 0));
}

TEST(FootballTeams, AddMatchResults_WinGame) {
    Team team {"TestTeam"};

    team.addMatchResults(3, 2, 3);
    ASSERT_EQ(team.name, "TestTeam");
    ASSERT_EQ(team.points, 3);
    ASSERT_EQ(team.gamesWonCount, 1);
    ASSERT_EQ(team.gamesDrawnCount, 0);
    ASSERT_EQ(team.gamesLostCount, 0);
    ASSERT_EQ(team.totalGoalsScored, 3);
    ASSERT_EQ(team.totalGoalsConceded, 2);
}

TEST(FootballTeams, AddMatchResults_LostGame) {
    Team team {"TestTeam"};

    team.addMatchResults(3, 1231, 0); // yes! total loss ))
    ASSERT_EQ(team.name, "TestTeam");
    ASSERT_EQ(team.points, 0);
    ASSERT_EQ(team.gamesWonCount, 0);
    ASSERT_EQ(team.gamesDrawnCount, 0);
    ASSERT_EQ(team.gamesLostCount, 1);
    ASSERT_EQ(team.totalGoalsScored, 3);
    ASSERT_EQ(team.totalGoalsConceded, 1231);
}

TEST(FootballTeams, AddMatchResults_Drawn) {
    Team team {"TestTeam"};

    team.addMatchResults(2, 2, 1); // yes! total loss ))
    ASSERT_EQ(team.name, "TestTeam");
    ASSERT_EQ(team.points, 1);
    ASSERT_EQ(team.gamesWonCount, 0);
    ASSERT_EQ(team.gamesDrawnCount, 1);
    ASSERT_EQ(team.gamesLostCount, 0);
    ASSERT_EQ(team.totalGoalsScored, 2);
    ASSERT_EQ(team.totalGoalsConceded, 2);
}

TEST(FootballTeams, FiveGamesTest) {
    Team team {"TestTeam"};

    team.addMatchResults(4, 2, 3); // Win!!!
    team.addMatchResults(2, 2, 1); // Drawn
    team.addMatchResults(0, 3, 0); // we've lost ((
    team.addMatchResults(3, 3, 1); // Drawn
    team.addMatchResults(1, 0, 3); // Win

    ASSERT_EQ(team.name, "TestTeam");
    ASSERT_EQ(team.points, 8);
    ASSERT_EQ(team.gamesWonCount, 2);
    ASSERT_EQ(team.gamesDrawnCount, 2);
    ASSERT_EQ(team.gamesLostCount, 1);
    ASSERT_EQ(team.totalGoalsScored, 10);
    ASSERT_EQ(team.totalGoalsConceded, 10);
}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}