cmake_minimum_required(VERSION 3.16 FATAL_ERROR)

project(TestTask)
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")
set(PROJECT_VERSION 1.0.0.0)
project(${PROJECT_NAME} VERSION ${PROJECT_VERSION} LANGUAGES CXX)

add_subdirectory(tests)
add_compile_options(-c -Wall -Wextra -O3 -std=c++2a)

list(APPEND APP_SOURCES
        ${CMAKE_CURRENT_LIST_DIR}/src/Team.cpp ${CMAKE_CURRENT_LIST_DIR}/src/Team.h
        ${CMAKE_CURRENT_LIST_DIR}/src/Utilities.cpp ${CMAKE_CURRENT_LIST_DIR}/src/Utilities.h
        ${CMAKE_CURRENT_LIST_DIR}/src/FootballTournament.cpp ${CMAKE_CURRENT_LIST_DIR}/src/FootballTournament.h
)

add_library(appLibrary STATIC ${APP_SOURCES})

# include all components
add_executable(TestTask src/main.cpp ${APP_SOURCES})
TARGET_LINK_LIBRARIES(TestTask)