//
// Created by andtokm on 27.02.2022.
//

#include <fstream>
#include <charconv>
#include <cmath>

#include "FootballTournament.h"

FootballTournament::FootballTournament(std::unique_ptr<Utilities::IPointsCalcStrategy> ptsStrategy):
        pointsStrategy {std::move(ptsStrategy)} {
}

/**
 * Computes the goal cumulative probability distribution.
 * Note:
 * - Allowed goals per match vary in the range [0, 5] (inclusive)
 * - std::map::key ----> Goal count
 * - std::map::value --> Cumulative probability
 *
 * @param meanGoalPerMatch   Arithmetic mean goal per match.
 * @return                   Probability distribution map.
 */
[[nodiscard("Do not forget to use value. It's not for free!")]]
std::map<uint32_t, double> FootballTournament::GetGoalDistribution(const double meanGoalPerMatch) noexcept
{
    constexpr auto lbdFactorial{ [](const uint32_t factorial) {
        auto result{ 1u };
        for (uint32_t idx = 1u; idx <= factorial; ++idx) {
            result *= idx;
        }
        return result;
    }};

    constexpr auto maxGoals{ 5u };

    auto prevProbability{ 0.0 };
    std::map<uint32_t, double> goalDist;
    for (uint32_t i = 0u; i < maxGoals; ++i)
    {
        const auto probPoisson{ (std::pow(meanGoalPerMatch, i) * std::exp(-1 * meanGoalPerMatch))
                                / lbdFactorial(i) };
        goalDist.emplace(std::make_pair(i, (probPoisson + prevProbability)));
        prevProbability += probPoisson;
    }

    goalDist.emplace(std::make_pair(maxGoals, 1.0));
    return goalDist;
}

/**
 * Create a list of commands by reading it from a .csv file
 * @param path  - absolute path to file
 * @return  True is case of success and False otherwise
 */
bool FootballTournament::RegisterTeams(std::string_view path) noexcept {
    std::vector<std::string> lines;
    if (std::fstream file = std::fstream (path.data()); file.is_open() && file.good()) {
        while (std::getline(file, lines.emplace_back())) { /* */ }
    } else {
        return false;
    }

    // We do expect that the first line of the file is the header
    if (lines.size() <= 1)
        return false;

    teamsList.clear();
    teamsList.reserve(lines.size() - 1);
    for (size_t i = 1; i < lines.size(); ++i) {
        const std::string& teamScoreStr { lines[i] };
        if (size_t pos = teamScoreStr.find(','); std::string::npos != pos) {
            double score {};
            const auto [ptr, errCode] { std::from_chars(teamScoreStr.data() + pos + 1,
                                                        teamScoreStr.data() + teamScoreStr.size(), score) };
            if (errCode == std::errc()) {
                auto& team = teamsList.emplace_back();
                team.name.assign(teamScoreStr, 0, pos);
                team.goalsMean = score;
            }
        }
    }
    teamsList.shrink_to_fit();
    return not teamsList.empty();
}

/**
 * The method calculates a certain probabilistic number of goals scored by one
 * team in a match depending on the statistical distribution
 *
 * @param distribution  Distribution of goals of this team
 * @param likelihood    a certain probability value for this match
 * @return              probable number of goals
 */
[[nodiscard("Do not forget to use value")]]
uint32_t FootballTournament::getGoalsScored(const std::map<uint32_t, double>& distribution,
                                            const double likelihood) noexcept {
    if (likelihood < 0)
        return 0;
    for (double prev = 0; const auto& [count, chance]: distribution) {
        if (prev <= likelihood && likelihood < chance)
            return count;
        prev = chance;
    }
    return distribution.rbegin()->first;
}

/**
 * The method stimulates a match between two teams based on random state (for each team)
 * and team goals statistical distribution
 *
 * @param fistTeam
 * @param secondTeam
 * @return           None
 */
void FootballTournament::SimulateMatch(Team& fistTeam,
                                       Team& secondTeam) const noexcept
{
    const std::map<uint32_t, double> distribution1 = GetGoalDistribution(fistTeam.goalsMean),
            distribution2 = GetGoalDistribution(secondTeam.goalsMean);
    const uint32_t firstTeamGoals = getGoalsScored(distribution1, Utilities::randomDouble()),
            secondTeamGoals = getGoalsScored(distribution2, Utilities::randomDouble());
    const auto points = pointsStrategy->getPoints(firstTeamGoals, secondTeamGoals);

    fistTeam.addMatchResults(firstTeamGoals, secondTeamGoals, points.first);
    secondTeam.addMatchResults(secondTeamGoals, firstTeamGoals, points.second);
}


void FootballTournament::StartCompetition() noexcept
{
    constexpr size_t maxAttempts { 2 };
    for (size_t teamsCount = teamsList.size(), i = 0; i < teamsCount -1; ++i) {
        for (size_t attempt = 0; attempt < maxAttempts; ++attempt) {
            for (size_t n = i + 1; n < teamsCount; n++)
                this->SimulateMatch(teamsList[i], teamsList[n]);
        }
    }
}


bool FootballTournament::WriteResultsToFile(std::string_view path) {
    std::sort(teamsList.begin(), teamsList.end(), [](const Team& t1, const Team& t2) {
        return t1.points > t2.points;;
    });

    if (std::fstream file = std::fstream (path.data(), std::ios::out); file.is_open() && file.good()) {
        file << "TeamName,Points,GamesWonCount,GamesDrawnCount,GamesLostCount,TotalGoalsScored,TotalGoalsConceded\n";
        for (const Team& team: teamsList) {
            file << team.name << ",";
            file << team.points << ",";
            file << team.gamesWonCount << ",";
            file << team.gamesDrawnCount << ",";
            file << team.gamesLostCount << ",";
            file << team.totalGoalsScored << ",";
            file << team.totalGoalsConceded << "\n";
        }
        return true;
    } else {
        return false;
    }
}
