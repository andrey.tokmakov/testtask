//============================================================================
// Name        : Utilities.h
// Created on  : 27.02.2022.
// Author      : Tokmakov Andrei
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Utilities
//============================================================================

#ifndef CPPPROJECTS_UTILITIES_H
#define CPPPROJECTS_UTILITIES_H

#include <string>

namespace Utilities
{
    /**
     * Interface for a group of classes implementing a strategy for calculating the number of
     * points awarded to teams depending on the number of points scored by them per match
     *
     * @param firstTeamGoals   number of goals scored by the first team
     * @param secondTeamGoals  number of goals scored by the second team
     * @return                 std::pair<size_t, size_t> where the
     *                         first  - is the number of points awarded to the first team and the
     *                         second - is the number of points to the second
     */
    struct IPointsCalcStrategy {
        [[nodiscard]]
        virtual std::pair<size_t, size_t> getPoints(size_t firstTeamGoals,
                                                    size_t secondTeamGoals) const noexcept = 0;
        virtual ~IPointsCalcStrategy() = default;
    };

    /**
     * Implementation of the classical algorithm for calculating team points depending on goals.
     * (the logic of the work is taken from the description of the test task)
     */
    struct PointsCalcStrategyImpl final : public IPointsCalcStrategy {
        [[nodiscard]]
        std::pair<size_t, size_t> getPoints(size_t firstTeamGoals,
                                            size_t secondTeamGoals) const noexcept override;
        ~PointsCalcStrategyImpl() override = default;
    };


    // Just generates pseudo-random numbers in the range
    double randomDouble(double from = 0.0, double until = 1.0) noexcept;
};

#endif //CPPPROJECTS_UTILITIES_H
