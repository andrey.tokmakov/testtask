//============================================================================
// Name        : main.cpp
// Created on  : 27.02.2022.
// Author      : Tokmakov Andrei
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : main
//============================================================================

#include <iostream>
#include "FootballTournament.h"

namespace
{
    void printHelp() {
        std::cout << "Usage example: ./TestTask <MEAN_GOAL_PER_GAME> <RESULT_FILE> \n"
                  << "    <MEAN_GOAL_PER_GAME> - csv file path containing the mean goal per game\n"
                  << "    <RESULT_FILE>        - csv file to store results\n";
    }
}

int main(int argc, char** argv)
{
    const std::vector<std::string_view> args(argv + 1, argv + argc);
    if (args.size() != 2) {
        printHelp();
        return EXIT_FAILURE;
    }

    FootballTournament tournament { std::make_unique<Utilities::PointsCalcStrategyImpl>() };
    if (tournament.RegisterTeams(args.front())) {
        tournament.StartCompetition();
        tournament.WriteResultsToFile(args.back());
    }
    return EXIT_SUCCESS;
}
