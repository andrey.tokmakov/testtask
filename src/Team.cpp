//============================================================================
// Name        : Team.cpp
// Created on  : 27.02.2022.
// Author      : Tokmakov Andrei
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Team
//============================================================================

#include "Team.h"

void Team::addMatchResults(size_t teamGoals,
                           size_t opponentsGoals,
                           size_t matchPoints) noexcept {
    totalGoalsScored += teamGoals;
    totalGoalsConceded += opponentsGoals;
    points += matchPoints;
    gamesWonCount += teamGoals > opponentsGoals ? 1 : 0;
    gamesDrawnCount += teamGoals == opponentsGoals ? 1 : 0;
    gamesLostCount += teamGoals < opponentsGoals ? 1 : 0;
}