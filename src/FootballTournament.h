//============================================================================
// Name        : FootballTournament.h
// Created on  : 27.02.2022.
// Author      : Tokmakov Andrei
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : FootballTournament
//============================================================================

#ifndef CPPPROJECTS_FOOTBALLTOURNAMENT_H
#define CPPPROJECTS_FOOTBALLTOURNAMENT_H

#include <string_view>
#include <vector>
#include <memory>
#include <map>

#include "Team.h"
#include "Utilities.h"

class FootballTournament {
private:
    std::unique_ptr<Utilities::IPointsCalcStrategy> pointsStrategy { nullptr };
    std::vector<Team> teamsList {};

public:
    explicit FootballTournament(std::unique_ptr<Utilities::IPointsCalcStrategy> ptsStrategy);

    [[nodiscard("Do not forget to use value. It's not for free!")]]
    static std::map<uint32_t, double> GetGoalDistribution(const double meanGoalPerMatch) noexcept;

    [[nodiscard("Do not forget to use value")]]
    static uint32_t getGoalsScored(const std::map<uint32_t, double>& distribution,
                                   const double likelihood) noexcept;

    bool RegisterTeams(std::string_view path) noexcept;

    void SimulateMatch(Team& fistTeam,
                       Team& secondTeam) const noexcept;

    void StartCompetition() noexcept;
    bool WriteResultsToFile(std::string_view path);
};

#endif //CPPPROJECTS_FOOTBALLTOURNAMENT_H
