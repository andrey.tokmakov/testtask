//============================================================================
// Name        : Utilities.cpp
// Created on  : 27.02.2022.
// Author      : Tokmakov Andrei
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Utilities
//============================================================================

#include <random>
#include "Utilities.h"

namespace {
    /** Will be used to obtain a seed for the random number engine **/
    std::random_device randomDevice {};

    /** Standard mersenne_twister_engine seeded with rd() **/
    std::mt19937 engine(randomDevice());
}

namespace Utilities {

    /**
     * Implementation of the classical algorithm for calculating team points depending on goals.
     * (the logic of the work is taken from the description of the test task)
     */
    [[nodiscard]]
    std::pair<size_t, size_t> PointsCalcStrategyImpl::getPoints(size_t firstTeamGoals,
                                                                size_t secondTeamGoals) const noexcept {
        if (firstTeamGoals > secondTeamGoals)
            return {3, 0};
        else if (firstTeamGoals == secondTeamGoals)
            return {1, 1};
        return {0, 3};
    }

    // Just generates pseudo-random numbers in the range
    double randomDouble(double from, double until) noexcept {
        return std::uniform_real_distribution<double>{from, until}(engine);
    }
}