//============================================================================
// Name        : Team.h
// Created on  : 27.02.2022.
// Author      : Tokmakov Andrei
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Team
//============================================================================

#ifndef CPPPROJECTS_TEAM_H
#define CPPPROJECTS_TEAM_H

#include <string>

struct Team final {
    std::string name;
    size_t points {0};
    size_t gamesWonCount {0};
    size_t gamesDrawnCount {0};
    size_t gamesLostCount {0};
    size_t totalGoalsScored {0};
    size_t totalGoalsConceded {0};
    double goalsMean {0};

    // TODO: Add src
    void addMatchResults(size_t teamGoals,
                         size_t opponentsGoals,
                         size_t matchPoints) noexcept ;
};


#endif //CPPPROJECTS_TEAM_H
