# TestTask

0. [Overview](#Overview)
1. [Download](#Download)
2. [Build](#Build)
3. [Install GoogleTest](#Install GoogleTest)
4. [Requirements](#Requirements)
5. [Run application](#Run)
6. [Tests ](#Tests)


<a name="Overview"></a>
## Overview
Simulate a complete football tournament  based on data extracted from the English Premiere League season
2018/2019 


<a name="Download"></a>
## Download project:
- git clone https://gitlab.com/andrey.tokmakov/testtask.git
- cd testtask

<a name="Requirements"></a>
## Project requirements
1. Linux (_Tested on Ubuntu 20.04.4 LTS_)
2. C++20
3. Cmake
4. GoogleTest
5. (gcc-11 or clang-14)


<a name="Install GoogleTest"></a>
## Instal GoogleTest:
- sudo apt-get install libgtest-dev
- (or refer https://google.github.io/googletest/quickstart-cmake.html)

<a name="Build"></a>
## How to build project:
- `mkdir build`
- `cd build`
- Setup compillers `export CC=gcc-11 CXX=g++-11` (for example)
- `cmake ..`
- `make all`

<a name="Run"></a>
## Run:
- **Usage**: `./TestTask <MEAN_GOAL_PER_GAME> <RESULT_FILE>`
- **Example**: `./TestTask ../data/MeanGoalPerGame.csv ../data/result.csv`

<a name="Tests"></a>
## How to run tests
- Move to the project folder and build it:
- `cd testtask` && `cd build` && `make`
- Run unit tests executable: `./tests/tests`
